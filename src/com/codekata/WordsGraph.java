package com.codekata;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordsGraph {

    private static final String FILE_NAME = "wordlist.txt";
    private String firstWord;
    private String lastWord;
    private List<String> wordsList;

    public WordsGraph(String firstWord, String lastWord) {
        this.firstWord = firstWord;
        this.lastWord = lastWord;
        this.wordsList = readWordsFromFile();
        throwExceptionIfWordsAreNotAvailable();
    }

    public List<String> findShortestChain() {

        Map<String, String> mapOfFollowingWords = new HashMap<>();

        Queue<String> queue = new LinkedList<>();
        queue.add(firstWord);

        String word;
        while (!queue.isEmpty() && !(word = queue.remove()).equals(lastWord)) {
            List<String> followingWords = getFollowingWords(word);
            for (String followingWord : followingWords) {
                if (!mapOfFollowingWords.containsKey(followingWord)) {
                    mapOfFollowingWords.put(followingWord, word);
                    queue.add(followingWord);
                }
            }
        }

        return getChain(mapOfFollowingWords);
    }

    private void throwExceptionIfWordsAreNotAvailable() {
        boolean wordsExistOnList = wordsList.contains(firstWord) && wordsList.contains(lastWord);
        if (!wordsExistOnList) {
            throw new RuntimeException("Words need to be taken from the list of words!");
        }
    }

    private List<String> readWordsFromFile() {

        wordsList = new ArrayList<>();

        try {
            Stream<String> stream = Files.lines(Paths.get(FILE_NAME), Charset.forName("Cp1252"));
            wordsList = stream
                    .filter(line -> line.length() == firstWord.length())
                    .collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return wordsList;
    }

    private int getWordDiff(String word1, String word2) {
        int charDiff = 0;
        for (int i = 0; i < word1.length(); i++) {
            if (word1.charAt(i) != word2.charAt(i)) {
                charDiff++;
            }
        }
        return charDiff;
    }


    private List<String> getFollowingWords(String word) {
        List<String> followingWords = new ArrayList<>();
        for (String currWord : wordsList) {
            if (getWordDiff(word, currWord) == 1) {
                followingWords.add(currWord);
            }
        }
        return followingWords;
    }


    private List<String> getChain(Map<String, String> mapOfFollowingWords) {
        if (!mapOfFollowingWords.containsKey(lastWord)) {
            System.out.println("There is no connection between words");
            return null;
        }

        List<String> chain = new ArrayList<>();
        String word = lastWord;
        while (!word.equals(firstWord)) {
            chain.add(0, word);
            word = mapOfFollowingWords.get(word);
        }

        chain.add(0, firstWord);
        return chain;
    }

}

