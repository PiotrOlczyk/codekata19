package com.codekata;

import java.util.List;

public class WordChains {
    public static void main(String[] args) {

        String firstWord = "cat";
        String lastWord = "dog";

        exceptionIfWordsHaveDifferentLength(firstWord, lastWord);

        WordsGraph wordsGraph = new WordsGraph(firstWord, lastWord);
        List<String> listOfResults = wordsGraph.findShortestChain();

        System.out.print(listOfResults != null ? listOfResults : "No result found");
    }

    private static void exceptionIfWordsHaveDifferentLength(String firstWord, String lastWord) {
        if(firstWord.length() != lastWord.length()){
            throw new RuntimeException("Both words need to have the same number of characters!");
        }
    }


}
