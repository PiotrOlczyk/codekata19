package test;

import com.codekata.WordsGraph;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class WordsGraphTest {


    @Test
    public void findsShortestChainsFromCatToDog() {
        WordsGraph wordsGraph = new WordsGraph("cat", "dog");

        List<String> shortestChain = wordsGraph.findShortestChain();

        Assert.assertEquals(Arrays.asList("cat", "cot", "cog", "dog"), shortestChain);
    }

    @Test
    public void findsShortestChainsFromRubyToCode() {
        WordsGraph wordsGraph = new WordsGraph("ruby", "code");

        List<String> shortestChain = wordsGraph.findShortestChain();

        Assert.assertEquals(Arrays.asList("ruby", "rube", "robe", "rode", "code"), shortestChain);

    }

    @Test
    public void failsOnNonExistingChain(){
        WordsGraph wordsGraph = new WordsGraph("suffumigate", "Kabardinian");

        List<String> shortestChain = wordsGraph.findShortestChain();

        Assert.assertNull(shortestChain);

    }

    @Test(expected = RuntimeException.class)
    public void throwExceptionForNonExistingWords(){
        new WordsGraph("someStrangeWord", "Kabardinian");

    }
}
